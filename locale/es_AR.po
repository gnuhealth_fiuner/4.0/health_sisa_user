#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:res.user,sisa_mode:"
msgid "Mode"
msgstr "Modo"

msgctxt "field:res.user,sisa_password:"
msgid "SISA Password"
msgstr "Clave SISA"

msgctxt "field:res.user,sisa_password_hidden:"
msgid "SISA Password Hidden"
msgstr "Clave SISA"

msgctxt "field:res.user,sisa_user:"
msgid "SISA User"
msgstr "Usuario SISA"

msgctxt "selection:res.user,sisa_mode:"
msgid "Production"
msgstr "Producción"

msgctxt "selection:res.user,sisa_mode:"
msgid "Testing"
msgstr "Pruebas"

msgctxt "view:res.user:"
msgid "SISA"
msgstr "SISA"
